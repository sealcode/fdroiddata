Categories:System
License:GPL-3.0
Web Site:
Source Code:https://github.com/gabm/TapAndTurn
Issue Tracker:https://github.com/gabm/TapAndTurn/issues

Auto Name:Tap 'n' Turn
Summary:Confirm before the screen orientation gets applied
Description:
Tired of enabling the screen rotation everytime you want landscape mode?
Automatic rotation always rotates the screen in the wrong moment? Then this App
is for you!

Instead of rotating the screen right away, it displays an icon which allows you
explicitly adjust the orientation.
.

Repo Type:git
Repo:https://github.com/gabm/TapAndTurn

Build:1.0.0,1
    disable=appt error
    commit=v1.0.0
    subdir=app
    gradle=yes

Build:2.0.0,2
    commit=v2.0.0
    subdir=app
    gradle=yes

Build:2.1.0,3
    commit=v2.1.0
    subdir=app
    gradle=yes

Build:2.1.1,4
    commit=v2.1.1
    subdir=app
    gradle=yes

Build:2.2.0-beta,5
    commit=1379ba86caf4a9f6fb503f31e8406083c399d10c
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.1.1
Current Version Code:4
